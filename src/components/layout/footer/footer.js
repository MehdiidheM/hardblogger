/* Vendor imports */
import React from 'react'
/* App imports */
import style from './footer.module.less'

const Footer = () => (
  <div className={style.container}>
    <p>Fait avec mon &hearts; tendre</p>
  </div>
)

export default Footer
